﻿using NUnit.Framework;
using SharpBank.Test.Data;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        [Test]
        public void CustomerSummary_OneCustomer_Success()
        {
            // Arrange
            string expectedCustomerSummary = TestData.GetExpectedCustomerSummaryBankWithOneCustomer();
            Bank bank = TestData.GetBankWithOneCustomer();
            
            // Act
            string actualCustomerSummary = bank.CustomerSummary();

            // Assert
            Assert.AreEqual(expectedCustomerSummary, actualCustomerSummary);
        }

        [Test]
        public void CustomerSummary_ThreeCustomers_Success()
        {
            // Arrange
            string expectedCustomerSummary = TestData.GetExpectedCustomerSummaryBankWithThreeCustomer();
            Bank bank = TestData.GetBankWithThreeCustomers();
            
            // Act
            string actualCustomerSummary = bank.CustomerSummary();

            // Assert
            Assert.AreEqual(expectedCustomerSummary, actualCustomerSummary);
        }

        [Test]
        public void CustomerSummary_WithoutCustomers_Success()
        {
            // Arrange
            string expectedCustomerSummary = TestData.GetExpectedCustomerSummaryBankWithoutCustomers();
            Bank bank = TestData.GetBankWithoutCustomers();
            
            // Act
            string actualCustomerSummary = bank.CustomerSummary();

            // Assert
            Assert.AreEqual(expectedCustomerSummary, actualCustomerSummary);
        }

        [Test]
        public void TotalInteresrt_WithOneCustomer_Success()
        {
            // Arrange
            double expectedTotalInterestPaid = TestData.GetExpectedTotalInterestPaidBankWithOneCustomer();
            Bank bank = TestData.GetBankWithOneCustomer();
            
            // Act
            double actualTotalInterestPaid = bank.TotalInterestPaid;

            // Assert
            Assert.AreEqual(expectedTotalInterestPaid, actualTotalInterestPaid, TestData.DOUBLE_DELTA);
        }

        [Test]
        public void TotalInteresrt_WithThreeCustomers_Success()
        {
            // Arrange
            double expectedTotalInterestPaid = TestData.GetExpectedTotalInterestPaidBankWithThreeCustomers();
            Bank bank = TestData.GetBankWithThreeCustomers();
            
            // Act
            double actualTotalInterestPaid = bank.TotalInterestPaid;

            // Assert
            Assert.AreEqual(expectedTotalInterestPaid, actualTotalInterestPaid, TestData.DOUBLE_DELTA);
        }

        [Test]
        public void TotalInteresrt_WithoutCustomers_Success()
        {
            // Arrange
            double expectedTotalInterestPaid = TestData.GetExpectedTotalInterestPaidBankWithoutCustomers();
            Bank bank = TestData.GetBankWithoutCustomers();
            
            // Act
            double actualTotalInterestPaid = bank.TotalInterestPaid;

            // Assert
            Assert.AreEqual(expectedTotalInterestPaid, actualTotalInterestPaid, TestData.DOUBLE_DELTA);
        }
    }
}
