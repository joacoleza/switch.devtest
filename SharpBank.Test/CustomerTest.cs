﻿using System;
using NUnit.Framework;
using SharpBank.Exceptions;
using SharpBank.Test.Data;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        [Test]
        public void TotalInterestEarned_OneAccount_Success()
        {
            // Arrange
            CheckingAccount checkingAccount = new CheckingAccount(1);
            checkingAccount.Deposit(100, TestData.TEST_DATE_TIME);
            Customer customer = new Customer("John");
            customer.OpenAccount(checkingAccount);
            double expectedTotalInterestEarned = 0.00027397260273972606;

            // Act
            checkingAccount.AccrueDailyInterest(TestData.TEST_DATE_TIME);
            double actualTotalInterestEarned = customer.TotalInterestEarned;

            // Assert
            Assert.AreEqual(expectedTotalInterestEarned, actualTotalInterestEarned, TestData.DOUBLE_DELTA);
        }

        [Test]
        public void TotalInterestEarned_ThreeAccounts_Success()
        {
            // Arrange
            Customer customer = new Customer("Bill");

            CheckingAccount checkingAccount = new CheckingAccount(1);
            checkingAccount.Deposit(100, TestData.TEST_DATE_TIME);
            customer.OpenAccount(checkingAccount);

            SavingsAccount savingsAccount = new SavingsAccount(2);
            savingsAccount.Deposit(1500, TestData.TEST_DATE_TIME);
            customer.OpenAccount(savingsAccount);

            MaxiSavingsAccount maxiSavingsAccount = new MaxiSavingsAccount(3);
            maxiSavingsAccount.Deposit(3000, TestData.TEST_DATE_TIME);
            customer.OpenAccount(maxiSavingsAccount);
            
            double expectedTotalInterestEarned = 0.41671232876712333;

            // Act
            checkingAccount.AccrueDailyInterest(TestData.TEST_DATE_TIME);
            maxiSavingsAccount.AccrueDailyInterest(TestData.TEST_DATE_TIME);
            savingsAccount.AccrueDailyInterest(TestData.TEST_DATE_TIME);
            double actualTotalInterestEarned = customer.TotalInterestEarned;

            // Assert
            Assert.AreEqual(expectedTotalInterestEarned, actualTotalInterestEarned, TestData.DOUBLE_DELTA);
        }

        [Test]
        public void TotalInterestEarned_WithoutAccounts_Success()
        {
            // Arrange
            Customer customer = new Customer("Henry");
            double expectedTotalInterestEarned = 0;

            // Act
            double actualTotalInterestEarned = customer.TotalInterestEarned;

            // Assert
            Assert.AreEqual(expectedTotalInterestEarned, actualTotalInterestEarned, TestData.DOUBLE_DELTA);
        }

        [Test]
        public void AddAccount_NullAcccount_Failure()
        {
            // Arrange
            Customer customer = new Customer("Henry");

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => customer.OpenAccount(null));
        }

        [Test]
        public void AddAccount_AlreadyUsedId_Failure()
        {
            // Arrange
            int accountId = 1;
            Customer customer = new Customer("Bill");
            CheckingAccount checkingAccount = new CheckingAccount(accountId);
            customer.OpenAccount(checkingAccount);

            SavingsAccount savingsAccount = new SavingsAccount(accountId);
            string expectedErrorMessage = $"Cannot add account with id {accountId} because the customer already have an account with that id";

            // Act & Assert
            BusinessException ex = Assert.Throws<BusinessException>(() => customer.OpenAccount(savingsAccount));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [Test]
        public void Transfer_SourceAccountNotFound_Failure()
        {
            // Arrange
            int invalidAccountId = 99;
            Customer customer = new Customer("John");
            CheckingAccount checkingAccount = new CheckingAccount(1);
            checkingAccount.Deposit(1000, TestData.TEST_DATE_TIME);
            customer.OpenAccount(checkingAccount);

            SavingsAccount savingsAccount = new SavingsAccount(2);
            savingsAccount.Deposit(200, TestData.TEST_DATE_TIME);
            customer.OpenAccount(savingsAccount);

            string expectedErrorMessage = $"The account with id {invalidAccountId} was not found";

            // Act & Assert
            BusinessException ex = Assert.Throws<BusinessException>(() => customer.Transfer(invalidAccountId, savingsAccount.AccountId, 1000, TestData.TEST_DATE_TIME));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [Test]
        public void Transfer_DestinationAccountNotFound_Failure()
        {
            // Arrange
            int invalidAccountId = 99;
            Customer customer = new Customer("John");
            CheckingAccount checkingAccount = new CheckingAccount(1);
            checkingAccount.Deposit(1000, TestData.TEST_DATE_TIME);
            customer.OpenAccount(checkingAccount);

            SavingsAccount savingsAccount = new SavingsAccount(2);
            savingsAccount.Deposit(200, TestData.TEST_DATE_TIME);
            customer.OpenAccount(savingsAccount);

            string expectedErrorMessage = $"The account with id {invalidAccountId} was not found";

            // Act & Assert
            BusinessException ex = Assert.Throws<BusinessException>(() => customer.Transfer(checkingAccount.AccountId, invalidAccountId, 1000, TestData.TEST_DATE_TIME));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [TestCase(0)]
        [TestCase(-100)]
        public void Transfer_InvalidAmount_Failure(int amount)
        {
            // Arrange
            Customer customer = new Customer("John");
            CheckingAccount checkingAccount = new CheckingAccount(1);
            checkingAccount.Deposit(1000, TestData.TEST_DATE_TIME);
            customer.OpenAccount(checkingAccount);

            SavingsAccount savingsAccount = new SavingsAccount(2);
            savingsAccount.Deposit(200, TestData.TEST_DATE_TIME);
            customer.OpenAccount(savingsAccount);

            string expectedErrorMessage = "Amount must be greater than zero";

            // Act & Assert
            BusinessException ex = Assert.Throws<BusinessException>(() => customer.Transfer(checkingAccount.AccountId, savingsAccount.AccountId, amount, TestData.TEST_DATE_TIME));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [Test]
        public void Transfer_Success()
        {
            // Arrange
            Customer customer = new Customer("John");
            CheckingAccount checkingAccount = new CheckingAccount(1);
            checkingAccount.Deposit(1000, TestData.TEST_DATE_TIME);
            customer.OpenAccount(checkingAccount);

            SavingsAccount savingsAccount = new SavingsAccount(2);
            savingsAccount.Deposit(200, TestData.TEST_DATE_TIME);
            customer.OpenAccount(savingsAccount);

            double customerInitialAmount = customer.Amount;

            // Act
            customer.Transfer(checkingAccount.AccountId, savingsAccount.AccountId, 1000, TestData.TEST_DATE_TIME);

            // Assert
            Assert.AreEqual(customerInitialAmount, customer.Amount, TestData.DOUBLE_DELTA);
            Assert.AreEqual(1200, savingsAccount.Amount, TestData.DOUBLE_DELTA);
            Assert.AreEqual(0, checkingAccount.Amount, TestData.DOUBLE_DELTA);
        }
    }
}
