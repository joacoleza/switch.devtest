﻿using System;
using System.IO;
using NUnit.Framework;

namespace SharpBank.Test.Data
{
    public static class TestData
    {
        public const double DOUBLE_DELTA = 1e-15;

        public static DateTime TEST_DATE_TIME = new DateTime(2019, 01, 01); 

        private static readonly string TEST_DATA_BASE_PATH = Path.Combine(TestContext.CurrentContext.TestDirectory, "Data");

        private static readonly string EXPECTED_CUSTOMER_STATEMENT_FILE_PATH = Path.Combine(TEST_DATA_BASE_PATH, "ExpectedCustomerStatement.txt");

        private static readonly string EXPECTED_CUSTOMER_SUMMARY_ONE_CUSTOMER_FILE_PATH = Path.Combine(TEST_DATA_BASE_PATH, "ExpectedCustomerSummary_OneCustomer.txt");
        private static readonly string EXPECTED_CUSTOMER_SUMMARY_THREE_CUSTOMERS_PATH = Path.Combine(TEST_DATA_BASE_PATH, "ExpectedCustomerSummary_ThreeCustomers.txt");
        private static readonly string EXPECTED_CUSTOMER_SUMMARY_WITHOUT_CUSTOMERS_PATH = Path.Combine(TEST_DATA_BASE_PATH, "ExpectedCustomerSummary_WithoutCustomers.txt");

        public static string GetExpectedCustomerStatement()
        {
            return File.ReadAllText(EXPECTED_CUSTOMER_STATEMENT_FILE_PATH);
        }

        public static Customer GetCustomerForStatement()
        {
            CheckingAccount checkingAccount = new CheckingAccount(1);
            SavingsAccount savingsAccount = new SavingsAccount(2);

            Customer henry = new Customer("Henry");
            henry.OpenAccount(checkingAccount);
            henry.OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0, TEST_DATE_TIME);
            checkingAccount.AccrueDailyInterest(TEST_DATE_TIME);
            savingsAccount.Deposit(4000.0, TEST_DATE_TIME);
            savingsAccount.AccrueDailyInterest(TEST_DATE_TIME);

            savingsAccount.Withdraw(200.0, TEST_DATE_TIME.AddDays(1));
            savingsAccount.AccrueDailyInterest(TEST_DATE_TIME.AddDays(1));
            checkingAccount.AccrueDailyInterest(TEST_DATE_TIME.AddDays(1));

            return henry;
        }

        public static CheckingAccount GetCheckingAccount()
        {
            CheckingAccount checkingAccount = new CheckingAccount(1);
            checkingAccount.Deposit(100.0, TEST_DATE_TIME);
            checkingAccount.AccrueDailyInterest(TEST_DATE_TIME);
            return checkingAccount;
        }

        public static SavingsAccount GetSavingsAccount()
        {
            SavingsAccount savingsAccount = new SavingsAccount(2);
            savingsAccount.Deposit(1500.0, TEST_DATE_TIME);
            savingsAccount.AccrueDailyInterest(TEST_DATE_TIME);
            return savingsAccount;
        }

        public static MaxiSavingsAccount GetMaxiSavingsAccount()
        {
            MaxiSavingsAccount maxiSavingsAccount = new MaxiSavingsAccount(3);
            maxiSavingsAccount.Deposit(3000.0, TEST_DATE_TIME);
            maxiSavingsAccount.AccrueDailyInterest(TEST_DATE_TIME);
            return maxiSavingsAccount;
        }

        public static string GetExpectedCustomerSummaryBankWithOneCustomer()
        {
            return File.ReadAllText(EXPECTED_CUSTOMER_SUMMARY_ONE_CUSTOMER_FILE_PATH);
        }

        public static double GetExpectedTotalInterestPaidBankWithOneCustomer()
        {
            return 0.00027397260273972606;
        }

        public static string GetExpectedCustomerSummaryBankWithThreeCustomer()
        {
            return File.ReadAllText(EXPECTED_CUSTOMER_SUMMARY_THREE_CUSTOMERS_PATH);
        }

        public static double GetExpectedTotalInterestPaidBankWithThreeCustomers()
        {
            return 0.41698630136986303;
        }

        public static string GetExpectedCustomerSummaryBankWithoutCustomers()
        {
            return File.ReadAllText(EXPECTED_CUSTOMER_SUMMARY_WITHOUT_CUSTOMERS_PATH);
        }

        public static double GetExpectedTotalInterestPaidBankWithoutCustomers()
        {
            return 0.0;
        }

        public static Bank GetBankWithOneCustomer()
        {
            Bank bank = new Bank();
            bank.AddCustomer(GetCustomerWithOneAccount());
            return bank;
        }

        public static Bank GetBankWithThreeCustomers()
        {
            Bank bank = new Bank();
            bank.AddCustomer(GetCustomerWithOneAccount());
            bank.AddCustomer(GetCustomerWithThreeAccounts());
            bank.AddCustomer(GetCustomerWithoutAccounts());
            return bank;
        }

        public static Bank GetBankWithoutCustomers()
        {
            return new Bank();
        }

        public static Customer GetCustomerWithOneAccount()
        {
            Customer john = new Customer("John");
            john.OpenAccount(GetCheckingAccount());
            return john;
        }

        public static Customer GetCustomerWithThreeAccounts()
        {
            Customer bill = new Customer("Bill");
            bill.OpenAccount(GetCheckingAccount());
            bill.OpenAccount(GetSavingsAccount());
            bill.OpenAccount(GetMaxiSavingsAccount());
            return bill;
        }

        public static Customer GetCustomerWithoutAccounts()
        {
            return new Customer("Henry");
        }

    }
}
