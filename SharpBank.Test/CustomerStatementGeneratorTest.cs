﻿using NUnit.Framework;
using SharpBank.Test.Data;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerStatementGeneratorTest
    {
        [Test]
        public void TestCustomerStatementGeneration_Success()
        {
            // Arrange
            string expectedCustomerStatement = TestData.GetExpectedCustomerStatement();
            Customer customer = TestData.GetCustomerForStatement();

            // Act 
            string actualCustomerStatement = CustomerStatementGenerator.Generate(customer);

            // Assert
            Assert.AreEqual(expectedCustomerStatement, actualCustomerStatement);
        }
    }
}
