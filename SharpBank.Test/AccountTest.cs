﻿using System;
using NUnit.Framework;
using SharpBank.Exceptions;
using SharpBank.Test.Data;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        [TestCase(1000, 0.0027397260273972603)]
        [TestCase(50000, 0.13698630136986303)]
        [TestCase(100000, 0.27397260273972607)]
        [TestCase(1500000, 4.1095890410958908)]
        [TestCase(2000000, 5.4794520547945211)]
        [TestCase(25000000, 68.493150684931507)]
        [TestCase(700000000, 1917.8082191780823)]
        public void CheckingAccount_YearlyInterestEarner_Success(double accountAmount, double expectedInterestEarned)
        {
            // Arrange
            CheckingAccount account = new CheckingAccount(1);
            account.Deposit(accountAmount, TestData.TEST_DATE_TIME);

            // Act
            account.AccrueDailyInterest(TestData.TEST_DATE_TIME);

            // Assert
            Assert.AreEqual(expectedInterestEarned, account.InterestEarned, TestData.DOUBLE_DELTA);
        }

        [TestCase(100, 0.00027397260273972606)]
        [TestCase(500, 0.0013698630136986302)]
        [TestCase(1000, 0.0027397260273972603)]
        [TestCase(1500, 0.0054794520547945206)]
        [TestCase(2000, 0.0082191780821917818)]
        [TestCase(10000000, 54.791780821917811)]
        public void SavingsAccount_InterestEarner_Success(double accountAmount, double expectedInterestEarned)
        {
            // Arrange
            SavingsAccount account = new SavingsAccount(1);
            account.Deposit(accountAmount, TestData.TEST_DATE_TIME);

            // Act
            account.AccrueDailyInterest(TestData.TEST_DATE_TIME);

            // Assert
            Assert.AreEqual(expectedInterestEarned, account.InterestEarned, TestData.DOUBLE_DELTA);
        }

        [TestCase(100, 0.013698630136986302, null)]
        [TestCase(100, 0.00027397260273972606, 0)]
        [TestCase(100, 0.00027397260273972606, 10)]
        [TestCase(100, 0.013698630136986302, 11)]
        [TestCase(2500, 0.34246575342465757, null)]
        [TestCase(2500, 0.0068493150684931512, 0)]
        [TestCase(2500, 0.0068493150684931512, 10)]
        [TestCase(2500, 0.34246575342465757, 11)]
        [TestCase(25000000, 3424.6575342465758, null)]
        [TestCase(25000000, 68.493150684931507, 0)]
        [TestCase(25000000, 68.493150684931507, 10)]
        [TestCase(25000000, 3424.6575342465758, 11)]
        public void MaxiSavingsAccount_InterestEarner_Success(double accountAmount, double expectedInterestEarned, int? daysWithoutWithdraws)
        {
            // Arrange
            MaxiSavingsAccount account = new MaxiSavingsAccount(1);
            account.Deposit(accountAmount, TestData.TEST_DATE_TIME.AddDays(-15));

            if (daysWithoutWithdraws.HasValue)
            {
                account.Deposit(100, TestData.TEST_DATE_TIME.AddDays(-daysWithoutWithdraws.Value));
                account.Withdraw(100, TestData.TEST_DATE_TIME.AddDays(-daysWithoutWithdraws.Value));
            }

            // Act
            account.AccrueDailyInterest(TestData.TEST_DATE_TIME);

            // Assert
            Assert.AreEqual(expectedInterestEarned, account.InterestEarned, TestData.DOUBLE_DELTA);
        }

        [TestCase(0)]
        [TestCase(-100)]
        public void Deposit_InvalidAmount_Failure(int amount)
        {
            // Arrange
            CheckingAccount account = new CheckingAccount(1);
            string expectedErrorMessage = "Amount must be greater than zero";

            // Act & Assert
            BusinessException ex = Assert.Throws<BusinessException>(() => account.Deposit(amount, TestData.TEST_DATE_TIME));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [TestCase(0)]
        [TestCase(-100)]
        public void Withdraw_InvalidAmount_Failure(int amount)
        {
            // Arrange
            SavingsAccount account = new SavingsAccount(1);
            string expectedErrorMessage = "Amount must be greater than zero";

            // Act & Assert
            BusinessException ex = Assert.Throws<BusinessException>(() => account.Withdraw(amount, TestData.TEST_DATE_TIME));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [Test]
        public void ComputeAmount_Success()
        {
            // Arrange
            double expectedAmount = 1700;
            MaxiSavingsAccount account = new MaxiSavingsAccount(1);
            account.Deposit(3000, TestData.TEST_DATE_TIME);
            account.Withdraw(1000, TestData.TEST_DATE_TIME);
            account.Withdraw(500, TestData.TEST_DATE_TIME);
            account.Deposit(200, TestData.TEST_DATE_TIME);

            // Act
            double actualAmount = account.Amount;

            // Assert
            Assert.AreEqual(expectedAmount, actualAmount, TestData.DOUBLE_DELTA);
        }

        [Test]
        public void Withdraw_MoreThanTotalAmount_Failure()
        {
            MaxiSavingsAccount account = new MaxiSavingsAccount(1);
            account.Deposit(100, TestData.TEST_DATE_TIME);
            string expectedErrorMessage = "It is not possible to withdraw 101 because the total amount in the account is 100";

            // Act & Assert
            BusinessException ex = Assert.Throws<BusinessException>(() => account.Withdraw(101, TestData.TEST_DATE_TIME));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [TestCase(0)]
        [TestCase(-10)]
        public void Create_InvalidId_Failure(int accountId)
        {
            // Arrange
            string expectedErrorMessage = $"The accountId {accountId} is invalid";

            // Act & Assert
            ArgumentException ex = Assert.Throws<ArgumentException>(() => new SavingsAccount(accountId));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }
    }
}
