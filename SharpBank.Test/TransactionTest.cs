﻿using System;
using System.Linq;
using NUnit.Framework;
using SharpBank.Exceptions;
using SharpBank.Test.Data;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [TestCase(0)]
        [TestCase(10)]
        public void Create_WithdrawWithInvalidAmount_Failure(int amount)
        {
            // Arrange
            string expectedErrorMessage = "The amount for withdraws must be less than zero";

            // Act & Assert
            ArgumentException ex = Assert.Throws<ArgumentException>(()=> new Transaction(amount, TestData.TEST_DATE_TIME, TransactionType.WITHDRAW));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [TestCase(0)]
        [TestCase(-10)]
        public void Create_DepositWithInvalidAmount_Failure(int amount)
        {
            // Arrange
            string expectedErrorMessage = "The amount for deposits must be greater than zero";

            // Act & Assert
            ArgumentException ex = Assert.Throws<ArgumentException>(() => new Transaction(amount, TestData.TEST_DATE_TIME, TransactionType.DEPOSIT));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [TestCase(0)]
        [TestCase(-10)]
        public void Create_InterestWithInvalidAmount_Failure(int amount)
        {
            // Arrange
            string expectedErrorMessage = "The amount for interests must be greater than zero";

            // Act & Assert
            ArgumentException ex = Assert.Throws<ArgumentException>(() => new Transaction(amount, TestData.TEST_DATE_TIME, TransactionType.INTEREST));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }

        [TestCase(10, TransactionType.DEPOSIT)]
        [TestCase(-10, TransactionType.WITHDRAW)]
        public void Create_WithInvalidDateTime_Failure(int amount, TransactionType transactionType)
        {
            // Arrange
            string expectedErrorMessage = "Transaction date must not be the default datetime value";

            // Act & Assert
            ArgumentException ex = Assert.Throws<ArgumentException>(() => new Transaction(amount, default, transactionType));
            Assert.AreEqual(expectedErrorMessage, ex.Message);
        }
    }
}
