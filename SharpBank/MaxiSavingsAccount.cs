﻿using System;
using System.Linq;
using SharpBank.Resources;

namespace SharpBank
{
    public class MaxiSavingsAccount : BaseAccount
    {
        private const double YEARLY_RATE_FOR_ACCOUNTS_WITHOUT_WITHDRAWS = 0.05;
        private const double YEARLY_RATE_FOR_ACCOUNTS_WITH_WITHDRAWS = 0.001;
        private const double WITHDRAW_WINDOW_IN_DAYS = 10;

        public MaxiSavingsAccount(int accountId) : base(accountId)
        {
        }

        public override string AccountTypeName => BankResource.MaxiSavingsAccountTypeName;

        protected override double CalculateDailyInterest(DateTime date)
        {
            if (HasNotWithdrawsInWindowTime(date))
            {
                return Amount * GetDailyInterestRateFromYearlyInterest(YEARLY_RATE_FOR_ACCOUNTS_WITHOUT_WITHDRAWS, date);
            }
            return Amount * GetDailyInterestRateFromYearlyInterest(YEARLY_RATE_FOR_ACCOUNTS_WITH_WITHDRAWS, date);
        }

        private static int DaysBetweenDates(DateTime startDate, DateTime endDate)
        {
            return (endDate.Date - startDate.Date).Days;
        }

        private bool HasNotWithdrawsInWindowTime(DateTime date)
        {
            return _transactions
                        .Where(t => t.Type.Equals(TransactionType.WITHDRAW))
                        .All(t => DaysBetweenDates(t.TransactionDate, date) > WITHDRAW_WINDOW_IN_DAYS);
        }
    }
}
