﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharpBank.Exceptions;

namespace SharpBank
{
    public class Customer
    {
        private readonly List<BaseAccount> _accounts;

        public Customer(string name)
        {
            Name = name;
            _accounts = new List<BaseAccount>();
        }

        public string Name { get; private set; }

        public double Amount => _accounts.Sum(a => a.Amount);
        public double TotalInterestEarned => _accounts.Sum(a => a.InterestEarned);

        public IEnumerable<BaseAccount> Accounts => _accounts.AsReadOnly();

        public void OpenAccount(BaseAccount account)
        {
            if (account == null)
            {
                throw new ArgumentNullException("The account must not be null");
            }

            if (_accounts.Any(a => a.AccountId.Equals(account.AccountId)))
            {
                throw new BusinessException($"Cannot add account with id {account.AccountId} because the customer already have an account with that id");
            }

            _accounts.Add(account);
        }

        public void Transfer(int sourceAccountId, int destinationAccountId, double amount, DateTime transactionTime)
        {
            BaseAccount sourceAccount = _accounts.FirstOrDefault(a => a.AccountId.Equals(sourceAccountId));
            if (sourceAccount == default)
            {
                throw new BusinessException($"The account with id {sourceAccountId} was not found");
            }

            BaseAccount destinationAccount = _accounts.FirstOrDefault(a => a.AccountId.Equals(destinationAccountId));
            if (destinationAccount == default)
            {
                throw new BusinessException($"The account with id {destinationAccountId} was not found");
            }

            if (amount <= 0)
            {
                throw new BusinessException("Amount must be greater than zero");
            }

            //TODO - the following lines should implement a transaction to keep consistency in case of failure
            sourceAccount.Withdraw(amount, transactionTime);
            destinationAccount.Deposit(amount, transactionTime);
        }
    }
}
