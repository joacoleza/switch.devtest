﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpBank.Resources;

namespace SharpBank
{
    public class Bank
    {
        private const char NEW_LINE_SYMBOL = '\n';

        private readonly List<Customer> _customers;

        public Bank()
        {
            _customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("The customer must not be null");
            }
            _customers.Add(customer);
        }

        public string CustomerSummary()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(BankResource.CustomerSummaryHeader);
            foreach (Customer customer in _customers)
            {
                builder.Append(NEW_LINE_SYMBOL);
                builder.Append(GetCustomerSummary(customer));
            }
            return builder.ToString();
        }

        private string GetCustomerSummary(Customer customer)
        {
            return $" - {customer.Name} ({customer.Accounts.Count()} {BankResource.Account})";
        }

        public double TotalInterestPaid => _customers.Sum(c => c.TotalInterestEarned);
    }
}
