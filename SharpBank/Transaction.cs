﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public Transaction(double amount, DateTime transactionDate, TransactionType type)
        {
            if (transactionDate.Equals(default))
            {
                throw new ArgumentException("Transaction date must not be the default datetime value");
            }
            TransactionDate = transactionDate;

            if (type.Equals(TransactionType.WITHDRAW) && amount >= 0)
            {
                throw new ArgumentException("The amount for withdraws must be less than zero");
            }

            if (type.Equals(TransactionType.DEPOSIT) && amount <= 0)
            {
                throw new ArgumentException("The amount for deposits must be greater than zero");
            }

            if (type.Equals(TransactionType.INTEREST) && amount <= 0)
            {
                throw new ArgumentException("The amount for interests must be greater than zero");
            }

            Type = type;
            Amount = amount;
        }

        public double Amount { get; private set; }
        public DateTime TransactionDate { get; private set; }
        public TransactionType Type { get; private set; }
    }
}
