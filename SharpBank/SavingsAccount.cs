﻿using System;
using SharpBank.Resources;

namespace SharpBank
{
    public class SavingsAccount : BaseAccount
    {
        private const double YEARLY_INITIAL_INTEREST_RATE = 0.001;
        private const double INITIAL_INTEREST_RATE_LIMIT = 1000;
        private const double YEARLY_REST_INTEREST_RATE = 0.002;

        public SavingsAccount(int accountId) : base(accountId)
        {
        }

        public override string AccountTypeName => BankResource.SavingsAccountTypeName;

        protected override double CalculateDailyInterest(DateTime date)
        {
            if (Amount <= INITIAL_INTEREST_RATE_LIMIT)
            {
                return Amount * GetDailyInterestRateFromYearlyInterest(YEARLY_INITIAL_INTEREST_RATE, date);
            }
            else
            {
                double initialInterestEarned = INITIAL_INTEREST_RATE_LIMIT * GetDailyInterestRateFromYearlyInterest(YEARLY_INITIAL_INTEREST_RATE, date);
                double restInterestEarned = (Amount - INITIAL_INTEREST_RATE_LIMIT) * GetDailyInterestRateFromYearlyInterest(YEARLY_REST_INTEREST_RATE, date);
                return initialInterestEarned + restInterestEarned;
            }
        }
    }
}
