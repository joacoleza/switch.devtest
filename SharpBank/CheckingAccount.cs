﻿using System;
using SharpBank.Resources;

namespace SharpBank
{
    public class CheckingAccount : BaseAccount
    {
        private const double YEARLY_INTEREST_RATE = 0.001;

        public CheckingAccount(int accountId) : base(accountId)
        {
        }

        public override string AccountTypeName => BankResource.CheckingAccountTypeName;

        protected override double CalculateDailyInterest(DateTime date)
        {
            return Amount * GetDailyInterestRateFromYearlyInterest(YEARLY_INTEREST_RATE, date);
        }
    }
}
