﻿namespace SharpBank
{
    public enum TransactionType
    {
        WITHDRAW = 0,
        DEPOSIT = 1,
        INTEREST = 3
    }
}
