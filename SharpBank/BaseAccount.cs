﻿using System;
using System.Collections.Generic;
using SharpBank.Exceptions;

namespace SharpBank
{
    public abstract class BaseAccount
    {
        private const int DAYS_IN_NORMAL_YEAR = 365; 
        private const int DAYS_IN_LEAP_YEAR = 365; 

        protected readonly List<Transaction> _transactions;

        protected BaseAccount(int accountId)
        {
            if (accountId <= 0)
            {
                throw new ArgumentException($"The accountId {accountId} is invalid");
            }
            AccountId = accountId;
            _transactions = new List<Transaction>();
            Amount = 0;
        }

        public abstract string AccountTypeName { get; }

        public IEnumerable<Transaction> Transactions => _transactions.AsReadOnly();

        public double Amount { get; private set; }

        public int AccountId { get; private set; }
        public double InterestEarned { get; private set; }

        protected abstract double CalculateDailyInterest(DateTime date);

        // TODO - make method idempotent validating that there are no transactions of type INTEREST for the given date in the _transactions List
        public void AccrueDailyInterest(DateTime date)
        {
            double dailyInterest = CalculateDailyInterest(date);
            InterestEarned += dailyInterest;
            Amount += dailyInterest;
            _transactions.Add(new Transaction(dailyInterest, date, TransactionType.INTEREST));
        }

        public void Deposit(double amount, DateTime transactionTime)
        {
            if (amount <= 0)
            {
                throw new BusinessException("Amount must be greater than zero");
            }
            _transactions.Add(new Transaction(amount, transactionTime, TransactionType.DEPOSIT));
            Amount += amount;
        }

        public void Withdraw(double amount, DateTime transactionTime)
        {
            if (amount <= 0)
            {
                throw new BusinessException("Amount must be greater than zero");
            }

            if (Amount < amount)
            {
                throw new BusinessException($"It is not possible to withdraw {amount} because the total amount in the account is {Amount}");
            }

            _transactions.Add(new Transaction(-amount, transactionTime, TransactionType.WITHDRAW));
            Amount -= amount;
        }

        protected static double GetDailyInterestRateFromYearlyInterest(double yearlyInterest, DateTime date)
        {
            int daysInTheYear = DateTime.IsLeapYear(date.Year) ? DAYS_IN_LEAP_YEAR : DAYS_IN_NORMAL_YEAR;
            return yearlyInterest / daysInTheYear;

        }
    }
}
