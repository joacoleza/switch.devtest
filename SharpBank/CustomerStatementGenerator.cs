﻿using System;
using System.Text;
using SharpBank.Resources;

namespace SharpBank
{
    public static class CustomerStatementGenerator
    {
        private const char NEW_LINE_SYMBOL = '\n';
        private const string AMOUNT_STRING_FORMAT = "${0:N2}";

        public static string Generate(Customer customer)
        {
            StringBuilder builder = new StringBuilder();

            builder = AppendCustomerHeader(builder, customer);

            builder.Append(NEW_LINE_SYMBOL);
            builder.Append(NEW_LINE_SYMBOL);
            builder = AppendAccountsSection(builder, customer);
            builder = AppendCustomerFooter(builder, customer);

            return builder.ToString();
        }

        private static StringBuilder AppendAccountsSection(StringBuilder builder, Customer customer)
        {
            foreach (BaseAccount account in customer.Accounts)
            {
                builder = AppendAccountHeader(builder, account);
                builder.Append(NEW_LINE_SYMBOL);
                builder = AppendTransactionsSection(builder, account);

                string accountStatementFooter = string.Format(BankResource.AccountStatementFooter, FormatAmount(account.Amount));

                builder.Append(accountStatementFooter);
                builder.Append(NEW_LINE_SYMBOL);
                builder.Append(NEW_LINE_SYMBOL);
            }

            return builder;
        }

        private static StringBuilder AppendTransactionsSection(StringBuilder builder, BaseAccount account)
        {
            foreach (Transaction transaction in account.Transactions)
            {
                string transactionType;
                switch (transaction.Type)
                {
                    case TransactionType.WITHDRAW:
                        transactionType = BankResource.WithdrawalTransactionTypeName;
                        break;
                    case TransactionType.DEPOSIT:
                        transactionType = BankResource.DepositTransactionTypeName;
                        break;
                    case TransactionType.INTEREST:
                        transactionType = BankResource.InterestTransactionTypeName;
                        break;
                    default:
                        throw new InvalidOperationException($"Invalid transaction type {transaction.Type}");
                }

                builder.Append($"  {transactionType} {FormatAmount(transaction.Amount)}");
                builder.Append(NEW_LINE_SYMBOL);
            }

            return builder;
        }

        private static StringBuilder AppendCustomerHeader(StringBuilder builder, Customer customer)
        {
            string customerStatementHeader = string.Format(BankResource.CustomerStatementHeader, customer.Name);
            builder.Append(customerStatementHeader);
            return builder;
        }

        private static StringBuilder AppendAccountHeader(StringBuilder builder, BaseAccount account)
        {
            string accountStatementHeader = string.Format(BankResource.AccountStatementHeader, account.AccountTypeName);
            builder.Append(accountStatementHeader);
            return builder;
        }

        private static StringBuilder AppendCustomerFooter(StringBuilder builder, Customer customer)
        {
            string customerStatementFooter = string.Format(BankResource.CustomerStatementFooter, FormatAmount(customer.Amount));
            builder.Append(customerStatementFooter);
            return builder;
        }

        // TODO - when the amount is too small this format does not work. A good example are the daily interests
        private static string FormatAmount(double amount)
        {
            return string.Format(AMOUNT_STRING_FORMAT, Math.Abs(amount));
        }
    }
}
